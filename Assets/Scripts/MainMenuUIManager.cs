using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUIManager : MonoBehaviour
{
    public void LaunchGame()
    {
        Debug.Log("Load Game Scene");
        SceneManager.LoadScene("Game");
    }

    public void QuitGame()
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }

    public void LaunchDeckEditor()
    {
        Debug.Log("No scene create for now");
    }
}
